#!/bin/bash

sleep 1s;

newip=`cat /etc/hosts|grep cluster.local|awk '{print $1}'`
myid=`cat /data/nodes.conf |grep myself|awk '{print $1}'`
    
echo $newip > /data-ips/$myid.txt
echo "refresh ip: $myid -> $newip";
