#!/bin/bash

if [ $# -ne 1 ]; then
    exit 1;
fi

while :
do
    echo "while";
    chkip=`cat /data-ips/$1.txt`
    if [ "$chkip"x == ""x ]; then
        sleep 1s;
    else
        ping -c1 $chkip;
        if [ $? -eq 0 ]; then
            oldip=`cat /data/nodes.conf |grep -E "^$1"|awk '{print $2}'|cut -d ":" -f1`
            if [ "$oldip"x == ""x ]; then
                echo "no old ip,please restart cluster";
                rm -fr /data/nodes.conf
                exit 1;
            else
                echo "oldip=$oldip and newip=$chkip"
                sed -i "s/$oldip/$chkip/g" /data/nodes.conf
                echo "done $1 $chkip";
                exit 0;
            fi
        else
            sleep 1s;
        fi
    fi
done
