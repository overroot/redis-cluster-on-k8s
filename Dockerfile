FROM 172.168.27.239/devops/redis:5.0.8

RUN set -eux; \
        apt-get update; \
	apt-get install -y --no-install-recommends iputils-ping; \
	rm -rf /var/lib/apt/lists/*;
       

COPY start.sh /usr/local/bin/
COPY writeip.sh /usr/local/bin/
COPY checkip.sh /usr/local/bin/

RUN set -eux; \
        chmod +x /usr/local/bin/start.sh; \
        chmod +x /usr/local/bin/writeip.sh; \
        chmod +x /usr/local/bin/checkip.sh;

RUN mkdir /data-ips
VOLUME /data-ips
